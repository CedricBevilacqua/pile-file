﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pile_File
{
    public class File
    {
        private List<int> contenant = new List<int>();

        public void enfiler(int element)
        {
            contenant.Add(element);
        }

        public int defiler()
        {
            int element = contenant[0];
            contenant.RemoveAt(0);
            return element;
        }
    }
}
