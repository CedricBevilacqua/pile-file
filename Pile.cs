﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pile_File
{
    public class Pile
    {
        private List<int> contenant = new List<int>();

        public void empiler(int element)
        {
            contenant.Add(element);
        }

        public int depiler()
        {
            int element = contenant[contenant.Count - 1];
            contenant.RemoveAt(contenant.Count - 1);
            return element;
        }
    }
}
